{
    'name': "仪表板增强",
    'version': '16.0.1.0.0',
    'summary': """
    仪表板增强模块，支持odoo13.14.15.16,自定义大屏仪表数据显示方式
""",
    'description': """ odoo二开，MES，ERP，WMS智能仓，手持终端，RFID技术应用等服务。技术支持（微信）：18143419820 """,
    'author': '18143419820',
    'company': 'odoo',
    'category': '工具/dashboard',
    'depends': ['base', 'web'],
    'data': [
        'views/dashboard_view.xml',
        'views/dynamic_block_view.xml',
        'views/dashboard_menu_view.xml',
        'security/ir.model.access.csv',
    ],
    'assets': {
        'web.assets_backend': [
            'odoo_cus_dashboard/static/src/js/dynamic_dashboard.js',
            'odoo_cus_dashboard/static/src/js/Chart.bundle.js',
            'odoo_cus_dashboard/static/src/scss/style.scss',
            'odoo_cus_dashboard/static/src/scss/css.css',
            'odoo_cus_dashboard/static/src/xml/dynamic_dashboard_template.xml',
        ],
    },
    'images': ['static/description/banner.png'],
    'license': "AGPL-3",
    'installable': True,
    'application': True,
}
